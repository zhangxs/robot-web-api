package main

import (
	"github.com/go-chassis/go-chassis"
	"github.com/go-mesh/openlogging"
	"github.com/kataras/iris"
	"robot-web-api/routes"
)

func main() {
	if err := chassis.Init(); err != nil {
		openlogging.Error(err.Error())
		return
	}

	app := iris.Default()

	app.Configure(routes.Configure)

	app.Run(iris.Addr(":8889"), iris.WithConfiguration(iris.YAML("./conf/iris.yaml")))
}
