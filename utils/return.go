package utils

import (
	"github.com/kataras/iris"
	"net/http"
)

type JSON struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func ReturnJSON(code int, message string, data interface{}, ctx iris.Context) {
	v := JSON{code, message, data}

	ctx.StatusCode(http.StatusOK)
	ctx.JSON(v)
}
