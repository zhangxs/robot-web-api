package rest

import (
	"context"
	"encoding/json"
	"github.com/go-chassis/go-chassis/client/rest"
	"github.com/go-chassis/go-chassis/core"
	"github.com/go-chassis/go-chassis/core/common"
	"io/ioutil"
	"robot-web-api/utils"
)

func Request(httpMethod, url string, body []byte, contentType ...string) (utils.JSON, error) {
	returnData := utils.JSON{}

	restInvoker := core.NewRestInvoker()
	req, err := rest.NewRequest(httpMethod, url, body)
	if err != nil {
		return returnData, err
	}
	b, _ := json.Marshal(map[string]string{common.HeaderSourceName: ""})
	req.Header.Set(common.HeaderXCseContent, string(b))
	if len(contentType) > 0 {
		for _, v := range contentType {
			req.Header.Set("Content-Type", v)
		}
	}
	resp, err := restInvoker.ContextDo(context.TODO(), req)
	if err != nil {
		return returnData, err
	}

	defer resp.Body.Close()

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return returnData, err
	}

	if err := json.Unmarshal(respBody, &returnData); err != nil {
		return returnData, err
	}

	return returnData, nil
}
