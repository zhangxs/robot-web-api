package services

import (
	"bytes"
	"io"
	"mime/multipart"
	"net/http"
	"robot-web-api/datasource/rest"
	"robot-web-api/utils"
)

const (
	LocalUploadURI      = "/local/upload"
)

type Upload struct {
	ServiceName string
}

func NewUpload() *Upload {
	return &Upload{
		ServiceName: "upload",
	}
}

func (u *Upload) GetUploadToken(body []byte) (utils.JSON, error) {
	url := "http://" + u.ServiceName + "/qiniu/getUploadToken"
	return rest.Request(http.MethodGet, url, body)
}

func (u *Upload) LocalUpload(file multipart.File, header *multipart.FileHeader) (utils.JSON, error) {
	returnData := utils.JSON{}
	url := "http://" + u.ServiceName + LocalUploadURI

	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)
	fileWriter, err := bodyWriter.CreateFormFile("file", header.Filename)
	if err != nil {
		return returnData, err
	}

	_, err = io.Copy(fileWriter, file)
	if err != nil {
		return returnData, err
	}
	contentType := bodyWriter.FormDataContentType()
	bodyWriter.Close()

	data, err := rest.Request(http.MethodPost, url, bodyBuf.Bytes(), contentType)
	if err != nil {
		return data, err
	}

	return data, nil
}
