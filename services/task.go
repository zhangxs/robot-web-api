package services

import (
	"encoding/json"
	"net/http"
	"robot-web-api/datasource/rest"
	"robot-web-api/entity"
	"robot-web-api/entity/scene"
	"robot-web-api/utils"
)

const (
	TaskListURI                    = "/task/list"             // 任务列表
	TaskCdrListURI                 = "/task/cdr/list"         // 任务话单列表
	TaskCdrList2URI                = "/task/cdr/list2"        // 任务话单列表
	TaskCdrOneURI                  = "/task/cdr/one"          // 任务话单详情
	CreateTaskURI                  = "/task/create"           // 创建任务
	GetSceneReleaseAllURI          = "/scene/getReleaseAll"   // 获取发布的话术列表
	CreateSceneURI                 = "/scene/create"          // 创建话术
	GetSceneAllURI                 = "/scene/getAll"          // 获取话术列表
	EditSceneURI                   = "/scene/edit"            // 编辑话术
	SceneReleaseURI                = "/scene/release"         // 发布话术
	GetSceneOneURI                 = "/scene/getOne"          // 获取一条话术
	GetSceneReleaseURI             = "/scene/getSceneRelease" // 获取单条话术的发布列表
	CreateGradeURI                 = "/scene/grade/create"    // 创建等级
	GetGradeAllURI                 = "/scene/grade/all"       // 获取等级列表
	TaskChangeStateURI             = "/task/state"            // 修改任务状态
	DashboardGetDay                = "/dashboard/day"
	DashboardGetMonth              = "/dashboard/month"
	DashboardGetConsumption        = "/dashboard/consumption"
	DashboardGetIntention          = "/dashboard/intention"
	DashboardGetHangupReason       = "/dashboard/hangup/reason"
	DashboardGetConnectedAndIntent = "/dashboard/connected/intent"
	DashboardGetReport             = "/dashboard/report"
	TaskEdit                       = "/task/edit"
	TaskCdrFollow                  = "/task/cdr/follow"
	TaskCdrSetGrade                   = "/task/cdr/setGrade"  // 手工设置等级
	TaskCdrSetRemark                  = "/task/cdr/setRemark" // 手工设置备注
)

type Task struct {
	ServiceName string
}

func NewTask() *Task {
	return &Task{
		ServiceName: "task",
	}
}

func (t *Task) GetTaskList(params *entity.TaskList) (utils.JSON, error) {
	reqBody, _ := json.Marshal(params)
	url := "http://" + t.ServiceName + TaskListURI
	return rest.Request(http.MethodGet, url, reqBody)
}

func (t *Task) GetTaskCdrOne(params *entity.TaskCdrOne) (utils.JSON, error) {
	reqBody, _ := json.Marshal(params)
	url := "http://" + t.ServiceName + TaskCdrOneURI
	return rest.Request(http.MethodGet, url, reqBody)
}

func (t *Task) GetTaskCdrList(params *entity.TaskCdrList) (utils.JSON, error) {
	reqBody, _ := json.Marshal(params)
	url := "http://" + t.ServiceName + TaskCdrListURI
	return rest.Request(http.MethodGet, url, reqBody)
}

func (t *Task) GetTaskCdrList2(params *entity.TaskCdrList2) (utils.JSON, error) {
	reqBody, _ := json.Marshal(params)
	url := "http://" + t.ServiceName + TaskCdrList2URI
	return rest.Request(http.MethodGet, url, reqBody)
}

func (t *Task) CreateTask(params *entity.TaskCreate) (utils.JSON, error) {
	reqBody, _ := json.Marshal(params)
	url := "http://" + t.ServiceName + CreateTaskURI
	return rest.Request(http.MethodPost, url, reqBody)
}

func (t *Task) GetSceneRleaseAll(companyId int) (utils.JSON, error) {
	reqBody, _ := json.Marshal(map[string]int{"companyId": companyId})
	url := "http://" + t.ServiceName + GetSceneReleaseAllURI
	return rest.Request(http.MethodGet, url, reqBody)
}

func (t *Task) CreateScene(params scene.Create) (utils.JSON, error) {
	reqBody, _ := json.Marshal(params)
	url := "http://" + t.ServiceName + CreateSceneURI
	return rest.Request(http.MethodPost, url, reqBody)
}

func (t *Task) GetSceneOne(companyId, sceneId int) (utils.JSON, error) {
	reqBody, _ := json.Marshal(map[string]int{"companyId": companyId, "sceneId": sceneId})
	url := "http://" + t.ServiceName + GetSceneOneURI
	return rest.Request(http.MethodGet, url, reqBody)
}

func (t *Task) GetSceneAll(params scene.GetAll) (utils.JSON, error) {
	reqBody, _ := json.Marshal(params)
	url := "http://" + t.ServiceName + GetSceneAllURI
	return rest.Request(http.MethodGet, url, reqBody)
}

func (t *Task) EditScene(params scene.Edit) (utils.JSON, error) {
	reqBody, _ := json.Marshal(params)
	url := "http://" + t.ServiceName + EditSceneURI
	return rest.Request(http.MethodPost, url, reqBody)
}

func (t *Task) SceneRelease(companyId, sceneId int) (utils.JSON, error) {
	reqBody, _ := json.Marshal(map[string]int{"companyId": companyId, "sceneId": sceneId})
	url := "http://" + t.ServiceName + SceneReleaseURI
	return rest.Request(http.MethodPost, url, reqBody)
}

func (t *Task) GetSceneRlease(companyId, sceneId int) (utils.JSON, error) {
	reqBody, _ := json.Marshal(map[string]int{"companyId": companyId, "sceneId": sceneId})
	url := "http://" + t.ServiceName + GetSceneReleaseURI
	return rest.Request(http.MethodGet, url, reqBody)
}

func (t *Task) CreateGrade(params scene.PostGrade) (utils.JSON, error) {
	reqBody, _ := json.Marshal(params.Params)
	url := "http://" + t.ServiceName + CreateGradeURI
	return rest.Request(http.MethodPost, url, reqBody)
}

func (t *Task) GetGradeAll(sceneId int) (utils.JSON, error) {
	reqBody, _ := json.Marshal(map[string]int{"sceneId": sceneId})
	url := "http://" + t.ServiceName + GetGradeAllURI
	return rest.Request(http.MethodGet, url, reqBody)
}

func (t *Task) TaskChangeState(body []byte) (utils.JSON, error) {
	url := "http://" + t.ServiceName + TaskChangeStateURI
	return rest.Request(http.MethodPost, url, body)
}

func (t *Task) DashboardGetDay(body []byte) (utils.JSON, error) {
	url := "http://" + t.ServiceName + DashboardGetDay
	return rest.Request(http.MethodGet, url, body)
}

func (t *Task) DashboardGetMonth(body []byte) (utils.JSON, error) {
	url := "http://" + t.ServiceName + DashboardGetMonth
	return rest.Request(http.MethodGet, url, body)
}

func (t *Task) DashboardGetConsumption(body []byte) (utils.JSON, error) {
	url := "http://" + t.ServiceName + DashboardGetConsumption
	return rest.Request(http.MethodGet, url, body)
}

func (t *Task) DashboardGetIntention(body []byte) (utils.JSON, error) {
	url := "http://" + t.ServiceName + DashboardGetIntention
	return rest.Request(http.MethodGet, url, body)
}

func (t *Task) DashboardGetHangupReason(body []byte) (utils.JSON, error) {
	url := "http://" + t.ServiceName + DashboardGetHangupReason
	return rest.Request(http.MethodGet, url, body)
}

func (t *Task) GetConnectedAndIntent(body []byte) (utils.JSON, error) {
	url := "http://" + t.ServiceName + DashboardGetHangupReason
	return rest.Request(http.MethodGet, url, body)
}

func (t *Task) DashboardGetConnectedAndIntent(body []byte) (utils.JSON, error) {
	url := "http://" + t.ServiceName + DashboardGetConnectedAndIntent
	return rest.Request(http.MethodGet, url, body)
}

func (t *Task) DashboardGetReport(body []byte) (utils.JSON, error) {
	url := "http://" + t.ServiceName + DashboardGetReport
	return rest.Request(http.MethodGet, url, body)
}

func (t *Task) TaskEdit(body []byte) (utils.JSON, error) {
	url := "http://" + t.ServiceName + TaskEdit
	return rest.Request(http.MethodPost, url, body)
}

func (t *Task) TaskCdrFollow(body []byte) (utils.JSON, error) {
	url := "http://" + t.ServiceName + TaskCdrFollow
	return rest.Request(http.MethodPost, url, body)
}

func (t *Task) TaskCdrSetGrade(body []byte) (utils.JSON, error) {
	url := "http://" + t.ServiceName + TaskCdrSetGrade
	return rest.Request(http.MethodPost, url, body)
}

func (t *Task) TaskCdrSetRemark(body []byte) (utils.JSON, error) {
	url := "http://" + t.ServiceName + TaskCdrSetRemark
	return rest.Request(http.MethodPost, url, body)
}
