package services

import (
	"encoding/json"
	"net/http"
	"robot-web-api/datasource/rest"
	"robot-web-api/entity"
	"robot-web-api/utils"
)

var (
	LoginURI                = "/user/login"
	ParseTokenURI           = "/user/parseToken"
	GetDeviceURI            = "/company/getDevice"
	GetUserInfoURI          = "/user/getUserInfo"
	GetTtsURI               = "/dictionary/tts"
	GetIntentionTypeListURI = "/company/intention/type/list"
	CreateIntentionTypeURI  = "/company/intention/type/create"
	UpdateIntentionTypeURI  = "/company/intention/type/update"
	DeleteIntentionTypeURI  = "/company/intention/type/delete"
	GetIntentionListURI     = "/company/intention/list"
	CreateIntentionURI      = "/company/intention/create"
	UpdateIntentionURI      = "/company/intention/update"
	DeleteIntentionURI      = "/company/intention/delete"
	CompanyCreateConf       = "/company/conf"
	CompanyGetConf          = "/company/conf"
	UserCreate              = "/user/create"
	UserList                = "/user/list"
	UserEdit                = "/user/edit"
	UserChangeState         = "/user/change/state"
	CompanyGetInfo          = "/company/info"
	GetCompanyConcurrent    = "/company/concurrent"
)

type UserCenter struct {
	ServiceName string

	GetAmountURI              string
	UserChangePassword        string
	CompanyCreateConfURI      string
	DictionaryGetHangupReason string
}

func NewUserCenter() *UserCenter {
	return &UserCenter{
		ServiceName: "user-center",

		GetAmountURI:              "/company/amount",
		UserChangePassword:        "/user/password",
		CompanyCreateConfURI:      "/company/conf",
		DictionaryGetHangupReason: "/dictionary/hangup/reason",
	}
}

func (u *UserCenter) Login(tokenForm entity.TokenForm) (utils.JSON, error) {
	reqBody, _ := json.Marshal(tokenForm)
	url := "http://" + u.ServiceName + LoginURI
	return rest.Request(http.MethodGet, url, reqBody)
}

func (u *UserCenter) ChangePassword(body []byte) (utils.JSON, error) {
	url := "http://" + u.ServiceName + u.UserChangePassword
	return rest.Request(http.MethodPost, url, body)
}

func (u *UserCenter) ParseToken(token string) (utils.JSON, error) {
	reqBody, _ := json.Marshal(map[string]string{"token": token})
	url := "http://" + u.ServiceName + ParseTokenURI
	return rest.Request(http.MethodGet, url, reqBody)
}

func (u *UserCenter) GetDevice(companyId int) (utils.JSON, error) {
	reqBody, _ := json.Marshal(map[string]int{"companyId": companyId})
	url := "http://" + u.ServiceName + GetDeviceURI
	return rest.Request(http.MethodGet, url, reqBody)
}

func (u *UserCenter) GetAmount(companyId int) (utils.JSON, error) {
	reqBody, _ := json.Marshal(map[string]int{"companyId": companyId})
	url := "http://" + u.ServiceName + u.GetAmountURI
	return rest.Request(http.MethodGet, url, reqBody)
}

func (u *UserCenter) GetUserInfo(userId int) (utils.JSON, error) {
	reqBody, _ := json.Marshal(map[string]int{"userId": userId})
	url := "http://" + u.ServiceName + GetUserInfoURI
	return rest.Request(http.MethodGet, url, reqBody)
}

func (u *UserCenter) GetTts() (utils.JSON, error) {
	url := "http://" + u.ServiceName + GetTtsURI
	return rest.Request(http.MethodGet, url, nil)
}

func (u *UserCenter) GetHangupReason() (utils.JSON, error) {
	url := "http://" + u.ServiceName + u.DictionaryGetHangupReason
	return rest.Request(http.MethodGet, url, nil)
}

func (u *UserCenter) GetIntentionTypeList(body []byte) (utils.JSON, error) {
	url := "http://" + u.ServiceName + GetIntentionTypeListURI
	return rest.Request(http.MethodGet, url, body)
}

func (u *UserCenter) CreateIntentionType(body []byte) (utils.JSON, error) {
	url := "http://" + u.ServiceName + CreateIntentionTypeURI
	return rest.Request(http.MethodPost, url, body)
}

func (u *UserCenter) UpdateIntentionType(body []byte) (utils.JSON, error) {
	url := "http://" + u.ServiceName + UpdateIntentionTypeURI
	return rest.Request(http.MethodPost, url, body)
}

func (u *UserCenter) DeleteIntentionType(body []byte) (utils.JSON, error) {
	url := "http://" + u.ServiceName + DeleteIntentionTypeURI
	return rest.Request(http.MethodPost, url, body)
}

func (u *UserCenter) GetIntentionList(body []byte) (utils.JSON, error) {
	url := "http://" + u.ServiceName + GetIntentionListURI
	return rest.Request(http.MethodGet, url, body)
}

func (u *UserCenter) CreateIntention(body []byte) (utils.JSON, error) {
	url := "http://" + u.ServiceName + CreateIntentionURI
	return rest.Request(http.MethodPost, url, body)
}

func (u *UserCenter) UpdateIntention(body []byte) (utils.JSON, error) {
	url := "http://" + u.ServiceName + UpdateIntentionURI
	return rest.Request(http.MethodPost, url, body)
}

func (u *UserCenter) DeleteIntention(body []byte) (utils.JSON, error) {
	url := "http://" + u.ServiceName + DeleteIntentionURI
	return rest.Request(http.MethodPost, url, body)
}

func (u *UserCenter) CompanyCreateConf(body []byte) (utils.JSON, error) {
	url := "http://" + u.ServiceName + CompanyCreateConf
	return rest.Request(http.MethodPost, url, body)
}

func (u *UserCenter) CompanyGetConf(body []byte) (utils.JSON, error) {
	url := "http://" + u.ServiceName + CompanyGetConf
	return rest.Request(http.MethodGet, url, body)
}

func (u *UserCenter) DashboardGetDay(body []byte) (utils.JSON, error) {
	url := "http://" + u.ServiceName + DashboardGetDay
	return rest.Request(http.MethodGet, url, body)
}

func (u *UserCenter) UserCreate(body []byte) (utils.JSON, error) {
	url := "http://" + u.ServiceName + UserCreate
	return rest.Request(http.MethodPost, url, body)
}

func (u *UserCenter) UserList(body []byte) (utils.JSON, error) {
	url := "http://" + u.ServiceName + UserList
	return rest.Request(http.MethodGet, url, body)
}

func (u *UserCenter) UserEdit(body []byte) (utils.JSON, error) {
	url := "http://" + u.ServiceName + UserEdit
	return rest.Request(http.MethodPost, url, body)
}

func (u *UserCenter) UserChangeState(body []byte) (utils.JSON, error) {
	url := "http://" + u.ServiceName + UserChangeState
	return rest.Request(http.MethodPost, url, body)
}

func (u *UserCenter) CompanyGetInfo(body []byte) (utils.JSON, error) {
	url := "http://" + u.ServiceName + CompanyGetInfo
	return rest.Request(http.MethodGet, url, body)
}

func (u *UserCenter) GetCompanyConcurrent(body []byte) (utils.JSON, error) {
	url := "http://" + u.ServiceName + GetCompanyConcurrent
	return rest.Request(http.MethodGet, url, body)
}
