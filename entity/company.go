package entity

type CompanyCreateIntentionType struct {
	CompanyId int
	TypeName  string
}

type CompanyUpdateIntentionType struct {
	Id        int
	CompanyId int
	TypeName  string
}

type CompanyDeleteIntentionType struct {
	Id        int
	CompanyId int
}

type CompanyCreateIntention struct {
	CompanyId       int
	IntentionTypeId int
	IntentionName   string
	Keyword         string
}

type CompanyUpdateIntention struct {
	Id              int
	CompanyId       int
	IntentionTypeId int
	IntentionName   string
	Keyword         string
}

type CompanyDeleteIntention struct {
	Id        int
	CompanyId int
}

type CompanyCreateConf struct {
	CompanyId     int
	StartCallTime string
	EndCallTime   string
	AutoCall      int
}

type CompanyGetInfo struct {
	CompanyId int
}
