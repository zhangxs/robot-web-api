package entity

import "github.com/gookit/validate"

type TokenForm struct {
	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required"`
	Token    string `json:"token" validate:"required"`
}

// Messages 您可以自定义验证器错误消息
func (t TokenForm) Messages() map[string]string {
	return validate.MS{
		"required": "{field}不能为空",
	}
}

func (t TokenForm) Translates() map[string]string {
	return validate.MS{
		"Username": "账号",
		"Password": "密码",
	}
}
