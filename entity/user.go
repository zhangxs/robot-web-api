package entity

type UserCreate struct {
	CompanyId      int
	Account        string
	Password       string
	Nickname       string
	Role           int
	ConcurrencyNum int
}

type UserEdit struct {
	UserId         int
	Password       string
	Nickname       string
	ConcurrencyNum int
}

type UserChangeState struct {
	UserId int
	State  int
}

type UserChangePassword struct {
	UserId      int
	OldPassword string
	NewPassword string
}
