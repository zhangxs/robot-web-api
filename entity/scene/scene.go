package scene

type SceneForm struct {
	Page      int
	PageSize  int
	CompanyId int
	UserId    int
	SceneId   int
	SceneName string
	SceneJson string
}

type Create struct {
	CompanyId int
	UserId    int
	SceneName string
	SceneJson string
}

type Edit struct {
	CompanyId int
	SceneId   int
	SceneName string
	SceneJson string
}

type GetAll struct {
	Page      int
	PageSize  int
	CompanyId int
	SceneName string
}

type Release struct {
	CompanyId int
	SceneId   int
}

type PostGrade struct {
	Params []struct{
		CompanyId int
		SceneId   int
		GradeName string
		ScoreMin  int
		ScoreMax  int
	}
}
