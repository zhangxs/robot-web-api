package company

type CreateIntentionType struct {
	CompanyId int
	TypeName  string
}

type UpdateIntentionType struct {
	Id        int
	CompanyId int
	TypeName  string
}

type DeleteIntentionType struct {
	Id        int
	CompanyId int
}

type CreateIntention struct {
	CompanyId       int
	IntentionTypeId int
	IntentionName   string
	Keyword         string
}

type UpdateIntention struct {
	Id            int
	CompanyId     int
	IntentionName string
	Keyword       string
}

type DeleteIntention struct {
	Id        int
	CompanyId int
}
