package entity

type TaskCreate struct {
	CompanyId      int
	UserId         int
	SceneReleaseId int
	DeviceId       int
	TaskName       string
	File           string
	Phones         string
	Remark         string
}

type TaskList struct {
	Page      int
	PageSize  int
	CompanyId int
	UserId    int
	SceneId   int
	TaskName  string
	StartTime string
	EndTime   string
	State     string
}

type TaskCdrOne struct {
	TaskId int
	Phone  string
}

type TaskCdrList struct {
	Page         int
	PageSize     int
	CompanyId    int
	UserId       int
	TaskId       int
	Phone        string
	IsConnect    string
	HangupReason string
	Grade        string
	MinDuration  int
	MaxDuration  int
	IsFollow     string
}

type TaskCdrList2 struct {
	Page         int
	PageSize     int
	CompanyId    int
	UserId       int
	TaskId       int
	Phone        string
	IsConnect    string
	HangupReason string
	Grade        string
	MinDuration  int
	MaxDuration  int
	Date         string
	IsFollow     string
	SceneId      int

	StartTime    string
	EndTime      string
}

type TaskChangeState struct {
	Id    int64
	State int
}

type TaskEdit struct {
	Id             int64
	DeviceId       int
	SceneReleaseId int
}

type TaskCdrFollow struct {
	TaskId int
	Phone  string
}

type TaskCdrSetGrade struct {
	TaskId    int
	Phone     string
	GradeName string
}

type TaskCdrSetRemark struct {
	TaskId int
	Phone  string
	Remark string
}
