package task

type Create struct {
	CompanyId      int
	UserId         int
	SceneReleaseId int
	DeviceId       int
	TaskName       string
	File           string
	Phones         string
	Remark         string
}

type List struct {
	Page      int
	PageSize  int
	CompanyId int
	UserId    int
	SceneId   int
	TaskName  string
	StartTime string
	EndTime   string
}

type CdrOne struct {
	TaskId int
	Phone  string
}

type CdrList struct {
	Page      int
	PageSize  int
	CompanyId int
	UserId    int
	TaskId    int
}

type CdrList2 struct {
	Page      int
	PageSize  int
	CompanyId int
	UserId    int
	TaskId    int
	Date      string
	Grade     string
}
