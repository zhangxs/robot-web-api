package entity

type DashboardGetDay struct {
	CompanyId int
	UserId    int
	Date      string
}

type DashboardGetMonth struct {
	CompanyId int
	UserId    int
	Date      string
}

type DashboardGetConsumption struct {
	CompanyId int
	UserId    int
	Date      string
}

type DashboardGetIntention struct {
	CompanyId int
	UserId    int
	Date      string
}

type DashboardGetHangupReason struct {
	CompanyId int
	UserId    int
	Date      string
}

type DashboardGetConnectedAndIntent struct {
	CompanyId int
	UserId    int
	Date      string
	TaskId    int
	SceneId   int
}

type DashboardGetReport struct {
	CompanyId int
	UserId    int
	Date      string
}
