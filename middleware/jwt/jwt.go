package jwt

import (
	"encoding/json"
	"github.com/kataras/iris"
	"github.com/kataras/iris/context"
	"robot-web-api/services"
	"robot-web-api/utils"
)

func New() context.Handler {
	return func(ctx context.Context) {
		jwtToken := ctx.GetHeader("Authorization")
		if jwtToken == "" {
			utils.ReturnJSON(iris.StatusForbidden, "没有权限", nil, ctx)
			return
		}

		resInfo, err := services.NewUserCenter().ParseToken(jwtToken)
		if err != nil {
			utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
			return
		}
		if resInfo.Code != iris.StatusOK {
			utils.ReturnJSON(iris.StatusInternalServerError, resInfo.Message, nil, ctx)
			return
		}

		userInfo := struct {
			CompanyId int
			UserId    int
			Nickname  string
			Role      int
		}{}
		v, _ := json.Marshal(resInfo.Data)
		if err := json.Unmarshal(v, &userInfo); err != nil {
			utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
			return
		}

		ctx.Values().Set("CompanyId", userInfo.CompanyId)
		ctx.Values().Set("UserId", userInfo.UserId)
		ctx.Values().Set("Nickname", userInfo.Nickname)
		ctx.Values().Set("Role", userInfo.Role)

		ctx.Next()
	}
}
