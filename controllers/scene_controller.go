package controllers

import (
	"github.com/kataras/iris"
	"robot-web-api/entity/scene"
	"robot-web-api/services"
	"robot-web-api/utils"
)

type SceneController struct {
}

func (s *SceneController) PostCreate(ctx iris.Context) {
	params := scene.Create{
		CompanyId: ctx.Values().Get("CompanyId").(int),
		UserId:    ctx.Values().Get("UserId").(int),
	}
	if err := ctx.ReadJSON(&params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}

	data, err := services.NewTask().CreateScene(params)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (s *SceneController) PostEdit(ctx iris.Context) {
	params := scene.Edit{
		CompanyId: ctx.Values().Get("CompanyId").(int),
	}
	if err := ctx.ReadJSON(&params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}

	data, err := services.NewTask().EditScene(params)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (s *SceneController) GetAll(ctx iris.Context) {
	params := scene.GetAll{
		Page:      ctx.URLParamIntDefault("page", 1),
		PageSize:  ctx.URLParamIntDefault("pageSize", 10),
		CompanyId: ctx.Values().Get("CompanyId").(int),
		SceneName: ctx.URLParam("sceneName"),
	}

	data, err := services.NewTask().GetSceneAll(params)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

// 获取发布的话术列表
func (s *SceneController) GetReleaseAll(ctx iris.Context) {
	companyId := ctx.Values().Get("CompanyId").(int)

	data, err := services.NewTask().GetSceneRleaseAll(companyId)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (s *SceneController) PostRelease(ctx iris.Context) {
	params := scene.Release{
		CompanyId: ctx.Values().Get("CompanyId").(int),
	}
	if err := ctx.ReadJSON(&params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}

	data, err := services.NewTask().SceneRelease(params.CompanyId, params.SceneId)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}


func (s *SceneController) GetOne(ctx iris.Context) {
	companyId := ctx.Values().Get("CompanyId").(int)
	sceneId, _ := ctx.URLParamInt("sceneId")

	data, err := services.NewTask().GetSceneOne(companyId, sceneId)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (s *SceneController) GetRelease(ctx iris.Context) {
	companyId := ctx.Values().Get("CompanyId").(int)
	sceneId, _ := ctx.URLParamInt("sceneId")

	data, err := services.NewTask().GetSceneRlease(companyId, sceneId)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (s *SceneController) PostGradeCreate(ctx iris.Context) {
	params := scene.PostGrade{}
	if err := ctx.ReadJSON(&params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}

	data, err := services.NewTask().CreateGrade(params)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (s *SceneController) GetGradeAll(ctx iris.Context) {
	sceneId := ctx.URLParamIntDefault("sceneId", 0)

	data, err := services.NewTask().GetGradeAll(sceneId)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}
