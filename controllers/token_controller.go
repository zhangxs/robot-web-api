package controllers

import (
	"github.com/gookit/validate"
	"github.com/kataras/iris"
	"robot-web-api/entity"
	"robot-web-api/services"
	"robot-web-api/utils"
)

type TokenController struct {
}

func (u *TokenController) Get(ctx iris.Context) {
	tokenForm := entity.TokenForm{
		Username: ctx.URLParam("username"),
		Password: ctx.URLParam("password"),
	}
	//if err := ctx.ReadJSON(&tokenForm); err != nil {
	//	utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
	//	return
	//}
	// 创建 Validation 实例
	v := validate.Struct(tokenForm, "login")
	v.WithScenes(validate.SValues{
		"login": []string{"username", "password"},
	})
	if !v.Validate() {
		utils.ReturnJSON(iris.StatusBadRequest, v.Errors.One(), nil, ctx)
		return
	}

	userCenter := services.NewUserCenter()
	data, err := userCenter.Login(tokenForm)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}
