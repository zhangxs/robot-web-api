package controllers

import (
	"encoding/json"
	"github.com/kataras/iris"
	"robot-web-api/services"
	"robot-web-api/utils"
)

type UploadController struct {
}

func (u *UploadController) GetToken(ctx iris.Context) {
	body, _ := json.Marshal(map[string]string{"bucket": "test"})
	data, err := services.NewUpload().GetUploadToken(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

// 上传文件
func (u *UploadController) Post(ctx iris.Context) {
	file, header, err := ctx.Request().FormFile("file")
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}
	defer file.Close()

	data, err := services.NewUpload().LocalUpload(file, header)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}
