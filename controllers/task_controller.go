package controllers

import (
	"encoding/json"
	"github.com/kataras/iris"
	"robot-web-api/entity"
	"robot-web-api/services"
	"robot-web-api/utils"
)

type TaskController struct {
}

func (t *TaskController) GetAll(ctx iris.Context) {
	params := &entity.TaskList{
		CompanyId: ctx.Values().Get("CompanyId").(int),
		UserId:    ctx.Values().Get("UserId").(int),
		Page:      ctx.URLParamIntDefault("page", 1),
		PageSize:  ctx.URLParamIntDefault("pageSize", 10),
		SceneId:   ctx.URLParamIntDefault("sceneId", 0),
		TaskName:  ctx.URLParamTrim("taskName"),
		StartTime: ctx.URLParamTrim("startTime"),
		EndTime:   ctx.URLParamTrim("endTime"),
		State:     ctx.URLParam("state"),
	}

	if role := ctx.Values().GetIntDefault("Role", 0); role == 1 { // 管理员
		params.UserId = ctx.URLParamIntDefault("userId", 0)
	}

	data, err := services.NewTask().GetTaskList(params)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (t *TaskController) GetCdrOne(ctx iris.Context) {
	params := &entity.TaskCdrOne{
		TaskId: ctx.URLParamIntDefault("taskId", 0),
		Phone:  ctx.URLParam("phone"),
	}

	data, err := services.NewTask().GetTaskCdrOne(params)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (t *TaskController) GetCdrAll(ctx iris.Context) {
	params := &entity.TaskCdrList{
		Page:         ctx.URLParamIntDefault("page", 1),
		PageSize:     ctx.URLParamIntDefault("pageSize", 10),
		CompanyId:    ctx.Values().Get("CompanyId").(int),
		UserId:       ctx.Values().Get("UserId").(int),
		TaskId:       ctx.URLParamIntDefault("taskId", 0),
		Phone:        ctx.URLParam("phone"),
		IsConnect:    ctx.URLParam("isConnect"),
		HangupReason: ctx.URLParam("hangupReason"),
		Grade:        ctx.URLParams()["grade[]"],
		MinDuration:  ctx.URLParamIntDefault("minDuration", 0),
		MaxDuration:  ctx.URLParamIntDefault("maxDuration", 0),
		IsFollow:     ctx.URLParamDefault("isFollow", ""),
	}

	if role := ctx.Values().GetIntDefault("Role", 0); role == 1 { // 管理员
		params.UserId = 0
	}

	data, err := services.NewTask().GetTaskCdrList(params)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (t *TaskController) GetCdrAll2(ctx iris.Context) {
	params := &entity.TaskCdrList2{
		CompanyId:    ctx.Values().Get("CompanyId").(int),
		UserId:       ctx.Values().Get("UserId").(int),
		Page:         ctx.URLParamIntDefault("page", 1),
		PageSize:     ctx.URLParamIntDefault("pageSize", 10),
		TaskId:       ctx.URLParamIntDefault("taskId", 0),
		Phone:        ctx.URLParam("phone"),
		IsConnect:    ctx.URLParam("isConnect"),
		HangupReason: ctx.URLParam("hangupReason"),
		Grade:        ctx.URLParams()["grade[]"],
		MinDuration:  ctx.URLParamIntDefault("minDuration", 0),
		MaxDuration:  ctx.URLParamIntDefault("maxDuration", 0),
		Date:         ctx.URLParamDefault("date", ""),
		IsFollow:     ctx.URLParamDefault("isFollow", ""),
		SceneId:      ctx.URLParamIntDefault("sceneId", 0),
		StartTime:    ctx.URLParam("startTime"),
		EndTime:      ctx.URLParam("endTime"),
	}
	if role := ctx.Values().GetIntDefault("Role", 0); role == 1 { // 管理员
		params.UserId = 0
	}

	data, err := services.NewTask().GetTaskCdrList2(params)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (t *TaskController) PostCreate(ctx iris.Context) {
	params := &entity.TaskCreate{
		CompanyId: ctx.Values().Get("CompanyId").(int),
		UserId:    ctx.Values().Get("UserId").(int),
	}
	if err := ctx.ReadJSON(&params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}

	data, err := services.NewTask().CreateTask(params)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (t *TaskController) PostEdit(ctx iris.Context) {
	params := &entity.TaskEdit{}
	if err := ctx.ReadJSON(&params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}

	body, err := json.Marshal(params)
	data, err := services.NewTask().TaskEdit(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (t *TaskController) PostChangeState(ctx iris.Context) {
	params := &entity.TaskChangeState{}
	if err := ctx.ReadJSON(&params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}

	body, err := json.Marshal(params)
	data, err := services.NewTask().TaskChangeState(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (t *TaskController) PostCdrFollow(ctx iris.Context) {
	params := &entity.TaskCdrFollow{}
	if err := ctx.ReadJSON(&params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}

	body, _ := json.Marshal(params)
	data, err := services.NewTask().TaskCdrFollow(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (t *TaskController) PostCdrGrade(ctx iris.Context) {
	params := &entity.TaskCdrSetGrade{}
	if err := ctx.ReadJSON(&params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}

	body, _ := json.Marshal(params)
	data, err := services.NewTask().TaskCdrSetGrade(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (t *TaskController) PostCdrRemark(ctx iris.Context) {
	params := &entity.TaskCdrSetRemark{}
	if err := ctx.ReadJSON(&params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}

	body, _ := json.Marshal(params)
	data, err := services.NewTask().TaskCdrSetRemark(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}
