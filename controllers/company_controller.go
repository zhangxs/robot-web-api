package controllers

import (
	"encoding/json"
	"github.com/kataras/iris"
	"robot-web-api/entity"
	"robot-web-api/services"
	"robot-web-api/utils"
)

type CompanyController struct{}

// 获取设备
func (c *CompanyController) GetDevice(ctx iris.Context) {
	companyId := ctx.Values().Get("CompanyId").(int)

	data, err := services.NewUserCenter().GetDevice(companyId)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

// 获取公司账户信息
func (c *CompanyController) GetAmount(ctx iris.Context) {
	companyId := ctx.Values().Get("CompanyId").(int)

	data, err := services.NewUserCenter().GetAmount(companyId)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (c *CompanyController) GetIntentionTypeList(ctx iris.Context) {
	companyId := ctx.Values().Get("CompanyId").(int)
	body, _ := json.Marshal(map[string]int{"companyId": companyId})
	data, err := services.NewUserCenter().GetIntentionTypeList(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}
	ctx.JSON(data)
}

func (c *CompanyController) PostIntentionTypeCreate(ctx iris.Context) {
	params := &entity.CompanyCreateIntentionType{}
	if err := ctx.ReadJSON(params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}
	params.CompanyId = ctx.Values().GetIntDefault("CompanyId", 0)

	body, _ := json.Marshal(params)
	data, err := services.NewUserCenter().CreateIntentionType(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}
	ctx.JSON(data)
}

func (c *CompanyController) PostIntentionTypeUpdate(ctx iris.Context) {
	params := &entity.CompanyUpdateIntentionType{}
	if err := ctx.ReadJSON(params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}
	params.CompanyId = ctx.Values().GetIntDefault("CompanyId", 0)

	body, _ := json.Marshal(params)
	data, err := services.NewUserCenter().UpdateIntentionType(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}
	ctx.JSON(data)
}

func (c *CompanyController) PostIntentionTypeDelete(ctx iris.Context) {
	params := &entity.CompanyDeleteIntentionType{}
	if err := ctx.ReadJSON(params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}
	params.CompanyId = ctx.Values().GetIntDefault("CompanyId", 0)

	body, _ := json.Marshal(params)
	data, err := services.NewUserCenter().DeleteIntentionType(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}
	ctx.JSON(data)
}

func (c *CompanyController) GetIntentionList(ctx iris.Context) {
	body, _ := json.Marshal(map[string]int{
		"companyId":       ctx.Values().Get("CompanyId").(int),
		"intentionTypeId": ctx.URLParamIntDefault("intentionTypeId", 0),
	})
	data, err := services.NewUserCenter().GetIntentionList(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}
	ctx.JSON(data)
}

func (c *CompanyController) PostIntentionCreate(ctx iris.Context) {
	params := &entity.CompanyCreateIntention{}
	if err := ctx.ReadJSON(params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}
	params.CompanyId = ctx.Values().GetIntDefault("CompanyId", 0)

	body, _ := json.Marshal(params)
	data, err := services.NewUserCenter().CreateIntention(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}
	ctx.JSON(data)
}

func (c *CompanyController) PostIntentionUpdate(ctx iris.Context) {
	params := &entity.CompanyUpdateIntention{}
	if err := ctx.ReadJSON(params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}
	params.CompanyId = ctx.Values().GetIntDefault("CompanyId", 0)

	body, _ := json.Marshal(params)
	data, err := services.NewUserCenter().UpdateIntention(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}
	ctx.JSON(data)
}

func (c *CompanyController) PostIntentionDelete(ctx iris.Context) {
	params := &entity.CompanyDeleteIntention{}
	if err := ctx.ReadJSON(params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}
	params.CompanyId = ctx.Values().GetIntDefault("CompanyId", 0)

	body, _ := json.Marshal(params)
	data, err := services.NewUserCenter().DeleteIntention(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}
	ctx.JSON(data)
}

func (c *CompanyController) GetConf(ctx iris.Context) {
	companyId := ctx.Values().GetIntDefault("CompanyId", 0)
	body, _ := json.Marshal(map[string]int{"companyId": companyId})

	data, err := services.NewUserCenter().CompanyGetConf(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}
	ctx.JSON(data)
}

func (c *CompanyController) PostConf(ctx iris.Context) {
	params := &entity.CompanyCreateConf{}
	if err := ctx.ReadJSON(params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}
	params.CompanyId = ctx.Values().GetIntDefault("CompanyId", 0)
	body, _ := json.Marshal(params)
	data, err := services.NewUserCenter().CompanyCreateConf(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}
	ctx.JSON(data)
}

func (c *CompanyController) GetInfo(ctx iris.Context) {
	params := &entity.CompanyGetInfo{
		CompanyId: ctx.Values().GetIntDefault("CompanyId", 0),
	}
	body, _ := json.Marshal(params)
	data, err := services.NewUserCenter().CompanyGetInfo(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}
	ctx.JSON(data)
}

func (c *CompanyController) GetConcurrent(ctx iris.Context) {
	params := &entity.CompanyGetInfo{
		CompanyId: ctx.Values().GetIntDefault("CompanyId", 0),
	}
	body, _ := json.Marshal(params)
	data, err := services.NewUserCenter().GetCompanyConcurrent(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}
	ctx.JSON(data)
}
