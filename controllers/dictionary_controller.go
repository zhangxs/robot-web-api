package controllers

import (
	"github.com/kataras/iris"
	"robot-web-api/services"
	"robot-web-api/utils"
)

type DictionaryController struct {
}

func (u *DictionaryController) GetTts(ctx iris.Context) {
	data, err := services.NewUserCenter().GetTts()
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (u *DictionaryController) GetHangupReason(ctx iris.Context) {
	data, err := services.NewUserCenter().GetHangupReason()
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}
