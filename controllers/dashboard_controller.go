package controllers

import (
	"encoding/json"
	"github.com/kataras/iris"
	"robot-web-api/entity"
	"robot-web-api/services"
	"robot-web-api/utils"
)

type DashboardController struct {
}

func (u *DashboardController) GetDay(ctx iris.Context) {
	params := &entity.DashboardGetDay{
		CompanyId: ctx.Values().GetIntDefault("CompanyId", 0),
		UserId:    ctx.Values().GetIntDefault("UserId", 0),
		Date:      ctx.URLParamTrim("date"),
	}
	if role := ctx.Values().GetIntDefault("Role", 0); role == 1 { // 管理员
		params.UserId = 0
	}

	body, _ := json.Marshal(params)
	data, err := services.NewTask().DashboardGetDay(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (u *DashboardController) GetMonth(ctx iris.Context) {
	params := &entity.DashboardGetMonth{
		CompanyId: ctx.Values().GetIntDefault("CompanyId", 0),
		UserId:    ctx.Values().GetIntDefault("UserId", 0),
		Date:      ctx.URLParamTrim("date"),
	}
	if role := ctx.Values().GetIntDefault("Role", 0); role == 1 { // 管理员
		params.UserId = 0
	}

	body, _ := json.Marshal(params)
	data, err := services.NewTask().DashboardGetMonth(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (u *DashboardController) GetConsumption(ctx iris.Context) {
	params := &entity.DashboardGetConsumption{
		CompanyId: ctx.Values().GetIntDefault("CompanyId", 0),
		UserId:    ctx.Values().GetIntDefault("UserId", 0),
		Date:      ctx.URLParamTrim("date"),
	}
	if role := ctx.Values().GetIntDefault("Role", 0); role == 1 { // 管理员
		params.UserId = 0
	}

	body, _ := json.Marshal(params)
	data, err := services.NewTask().DashboardGetConsumption(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (u *DashboardController) GetIntention(ctx iris.Context) {
	params := &entity.DashboardGetIntention{
		CompanyId: ctx.Values().GetIntDefault("CompanyId", 0),
		UserId:    ctx.Values().GetIntDefault("UserId", 0),
		Date:      ctx.URLParamTrim("date"),
	}
	if role := ctx.Values().GetIntDefault("Role", 0); role == 1 { // 管理员
		params.UserId = 0
	}

	body, _ := json.Marshal(params)
	data, err := services.NewTask().DashboardGetIntention(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (u *DashboardController) GetHangupReason(ctx iris.Context) {
	params := &entity.DashboardGetHangupReason{
		CompanyId: ctx.Values().GetIntDefault("CompanyId", 0),
		UserId:    ctx.Values().GetIntDefault("UserId", 0),
		Date:      ctx.URLParamTrim("date"),
	}
	if role := ctx.Values().GetIntDefault("Role", 0); role == 1 { // 管理员
		params.UserId = 0
	}

	body, _ := json.Marshal(params)
	data, err := services.NewTask().DashboardGetHangupReason(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (u *DashboardController) GetConnectedIntent(ctx iris.Context) {
	params := &entity.DashboardGetConnectedAndIntent{
		CompanyId: ctx.Values().GetIntDefault("CompanyId", 0),
		UserId:    ctx.Values().GetIntDefault("UserId", 0),
		Date:      ctx.URLParamTrim("date"),
		TaskId:    ctx.URLParamIntDefault("taskId", 0),
		SceneId:   ctx.URLParamIntDefault("sceneId", 0),
	}
	if role := ctx.Values().GetIntDefault("Role", 0); role == 1 { // 管理员
		params.UserId = 0
	}

	body, _ := json.Marshal(params)
	data, err := services.NewTask().DashboardGetConnectedAndIntent(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (u *DashboardController) GetReport(ctx iris.Context) {
	params := &entity.DashboardGetReport{
		CompanyId: ctx.Values().GetIntDefault("CompanyId", 0),
		UserId:    ctx.Values().GetIntDefault("UserId", 0),
		Date:      ctx.URLParamTrim("date"),
	}
	if role := ctx.Values().GetIntDefault("Role", 0); role == 1 { // 管理员
		params.UserId = 0
	}

	body, _ := json.Marshal(params)
	data, err := services.NewTask().DashboardGetReport(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}
