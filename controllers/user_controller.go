package controllers

import (
	"encoding/json"
	"github.com/kataras/iris"
	"robot-web-api/entity"
	"robot-web-api/services"
	"robot-web-api/utils"
)

type UserController struct {
}

// 获取用户信息
func (u *UserController) GetInfo(ctx iris.Context) {
	userId := ctx.Values().Get("UserId").(int)

	data, err := services.NewUserCenter().GetUserInfo(userId)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (u *UserController) GetList(ctx iris.Context) {
	companyId := ctx.Values().GetIntDefault("CompanyId", 0)
	body, _ := json.Marshal(map[string]int{"companyId": companyId})

	data, err := services.NewUserCenter().UserList(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (u *UserController) PostCreate(ctx iris.Context) {
	params := &entity.UserCreate{}
	if err := ctx.ReadJSON(params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}
	// 不是管理员不能创建
	if role := ctx.Values().GetIntDefault("Role", 0); role != 1 {
		utils.ReturnJSON(iris.StatusBadRequest, "没有权限", nil, ctx)
		return
	}
	params.CompanyId = ctx.Values().GetIntDefault("CompanyId", 0)
	params.Role = 2

	body, _ := json.Marshal(params)
	data, err := services.NewUserCenter().UserCreate(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (u *UserController) PostEdit(ctx iris.Context) {
	params := &entity.UserEdit{}
	if err := ctx.ReadJSON(params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}
	// 不是管理员不能修改
	if role := ctx.Values().GetIntDefault("Role", 0); role != 1 {
		utils.ReturnJSON(iris.StatusBadRequest, "没有权限", nil, ctx)
		return
	}

	body, _ := json.Marshal(params)
	data, err := services.NewUserCenter().UserEdit(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (u *UserController) PostChangeState(ctx iris.Context) {
	params := &entity.UserChangeState{}
	if err := ctx.ReadJSON(params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}
	// 不是管理员不能修改
	if role := ctx.Values().GetIntDefault("Role", 0); role != 1 {
		utils.ReturnJSON(iris.StatusBadRequest, "没有权限", nil, ctx)
		return
	}

	body, _ := json.Marshal(params)
	data, err := services.NewUserCenter().UserChangeState(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}

func (u *UserController) PostChangePassword(ctx iris.Context) {
	params := &entity.UserChangePassword{}
	if err := ctx.ReadJSON(params); err != nil {
		utils.ReturnJSON(iris.StatusBadRequest, err.Error(), nil, ctx)
		return
	}
	params.UserId = ctx.Values().GetIntDefault("UserId", 0)

	body, _ := json.Marshal(params)
	data, err := services.NewUserCenter().ChangePassword(body)
	if err != nil {
		utils.ReturnJSON(iris.StatusInternalServerError, err.Error(), nil, ctx)
		return
	}

	ctx.JSON(data)
}
