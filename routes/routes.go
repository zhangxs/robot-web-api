package routes

import (
	"github.com/iris-contrib/middleware/cors"
	"github.com/kataras/iris"
	"github.com/kataras/iris/mvc"
	"robot-web-api/controllers"
	"robot-web-api/middleware/jwt"
)

func Configure(app *iris.Application) {
	// 跨域
	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"}, //允许通过的主机名称
		AllowedHeaders:   []string{"*"},
		AllowCredentials: true,
	})

	v1 := app.Party("/", cors).AllowMethods(iris.MethodOptions)
	{
		// 登录注册
		mvc.New(v1.Party("/token")).Handle(new(controllers.TokenController))

		v1Sub := v1.Party("/", jwt.New())
		{
			// 字典数据
			mvc.New(v1Sub.Party("/dictionary")).Handle(new(controllers.DictionaryController))
			// 上传
			mvc.New(v1Sub.Party("/upload")).Handle(new(controllers.UploadController))
			// 公司相关
			mvc.New(v1Sub.Party("/company")).Handle(new(controllers.CompanyController))
			// 用户相关
			mvc.New(v1Sub.Party("/user")).Handle(new(controllers.UserController))
			// 任务相关
			mvc.New(v1Sub.Party("/task")).Handle(new(controllers.TaskController))
			// 话术相关
			mvc.New(v1Sub.Party("/scene")).Handle(new(controllers.SceneController))
			// 统计相关
			mvc.New(v1Sub.Party("/dashboard")).Handle(new(controllers.DashboardController))
		}
	}
}
